﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

using OpenQA.Selenium.Support.UI;
using System.Collections.Generic;
using System.IO;
using static System.Net.Mime.MediaTypeNames;

namespace FirstProject
{
    [TestClass]
    public class SeleniumTest
    {
        [TestMethod]
        public void TestMethod1()
        {
            IWebDriver driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl("https://www.google.com/");

            IWebElement elementSearch = driver.FindElement(By.Name("q"));

            elementSearch.SendKeys("Cheese");
            elementSearch.Submit();

            IWebElement imagePage = driver.FindElement(By.CssSelector("[href*=\"lnms\"]"));
            imagePage.Click();

            List<IWebElement> webElements = new List<IWebElement>();

            webElements.AddRange(driver.FindElement(By.Id("islmp")).FindElements(By.TagName("img")));

            WebDriverWait wait = new WebDriverWait(driver, System.TimeSpan.FromSeconds(10));
            wait.Until((d) => { return d.Title.ToLower().StartsWith("cheese"); });

            System.Console.WriteLine("Page title is -> " + driver.Title + " Count -> "+webElements.Count);

            ITakesScreenshot screenshotDriver = driver as ITakesScreenshot;
            Screenshot screenshot = screenshotDriver.GetScreenshot();
            screenshot.SaveAsFile("test.png", ScreenshotImageFormat.Png);

            driver.Quit();

            Assert.IsTrue(webElements.Count > 0);
        }
    }
}
